package com.gps.miguelfrausto.separategpsexamples;

import android.graphics.Color;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.widget.Button;
import android.widget.Switch;

public class ConfigActivity extends AppCompatActivity {

    public Switch autoCenter;
    public Switch autoZoom;
    public Button saveConfig;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_config);

        autoCenter = findViewById(R.id.sw_centrado_auto);
        autoZoom = findViewById(R.id.sw_auto_zoom);
        saveConfig = findViewById(R.id.btn_config_save);

        saveConfig.setOnClickListener(saveConfigListener);

    }

    View.OnClickListener saveConfigListener = new View.OnClickListener() {
        @Override
        public void onClick(View view) {
            Log.e(this.getClass().getName(), "Gurdando configuraciones...");
            Log.e(this.getClass().getName(), "Auto-centrado : " + autoCenter.isChecked());
            Log.e(this.getClass().getName(), "Auto-zoom : " + autoZoom.isChecked());
        }
    };
}
