package com.gps.miguelfrausto.separategpsexamples;

import android.os.Bundle;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;

import okhttp3.Call;
import okhttp3.Callback;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;

/**
 * Created by miguel frausto on 2/13/18.
 */

public class RunnableAPIDataActivity extends AppCompatActivity {

    public String url= "http://gps.frausto.mx/api/logs/last";
    public Handler handler = new Handler();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        /*
        try {
            run();
        } catch (IOException e) {
            e.printStackTrace();
        }
        */

        handler.post(getData);
    }

    public void publishResults(String result) throws JSONException {
        JSONObject Jobject = new JSONObject(result);
        Log.e("RunnableAPIDataActivity", "Published result: " + Jobject.get("id"));
    }

    private final Runnable getData = new Runnable() {
        @Override
        public void run() {
            try {
                OkHttpClient client = new OkHttpClient();

                Request request = new Request.Builder()
                        .url(url)
                        .build();

                client.newCall(request).enqueue(new Callback() {
                    @Override
                    public void onFailure(Call call, IOException e) {
                        call.cancel();
                    }

                    @Override
                    public void onResponse(Call call, Response response) throws IOException {

                        final String myResponse = response.body().string();


                        RunnableAPIDataActivity.this.runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                try {
                                    publishResults(myResponse);
                                } catch (JSONException e) {
                                    e.printStackTrace();
                                }
                            }
                        });

                    }
                });

                handler.postDelayed(this, 1000);
            }
            catch (Exception e) {
                e.printStackTrace();
            }
        }
    };

}
