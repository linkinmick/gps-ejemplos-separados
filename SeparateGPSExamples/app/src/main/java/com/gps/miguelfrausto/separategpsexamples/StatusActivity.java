package com.gps.miguelfrausto.separategpsexamples;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import java.io.IOException;

import okhttp3.Call;
import okhttp3.Callback;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;

public class StatusActivity extends AppCompatActivity {

    public String url= "http://gps.frausto.mx/api/logs/isOnline";
    public ImageView onIcon;
    public ImageView offIcon;
    public TextView connectionStatus;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_status);

        Toolbar myToolbar = (Toolbar) findViewById(R.id.gps_app_toolbar);
        setSupportActionBar(myToolbar);
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        onIcon = findViewById(R.id.online_icon);
        offIcon = findViewById(R.id.offline_icon);
        connectionStatus = findViewById(R.id.connection_status);

        try {
            run();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    void run() throws IOException {

        OkHttpClient client = new OkHttpClient();

        Request request = new Request.Builder()
                .url(url)
                .build();

        connectionStatus.setText("Verificando conexión con servidor...");

        client.newCall(request).enqueue(new Callback() {
            @Override
            public void onFailure(Call call, IOException e) {
                call.cancel();
            }

            @Override
            public void onResponse(Call call, Response response) throws IOException {

                final String apiResponse = response.body().string();


                StatusActivity.this.runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        boolean response = Boolean.parseBoolean(apiResponse);
                        handleStatus(response);
                    }
                });


                Log.e("GetAPIDataActivity", apiResponse);
            }
        });
    }

    public void handleStatus(Boolean isOnline) {
        if(isOnline){
            onIcon.setVisibility(View.VISIBLE);
            offIcon.setVisibility(View.GONE);
        } else {
            offIcon.setVisibility(View.VISIBLE);
            onIcon.setVisibility(View.GONE);
            Toast.makeText(this, "Servidor no disponible", Toast.LENGTH_LONG).show();
        }
        connectionStatus.setVisibility(View.GONE);
    }
}
