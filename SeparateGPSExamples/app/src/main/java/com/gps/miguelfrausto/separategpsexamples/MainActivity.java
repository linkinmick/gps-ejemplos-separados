package com.gps.miguelfrausto.separategpsexamples;

import android.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

public class MainActivity extends AppCompatActivity {


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(MainActivity.this);
        alertDialogBuilder.setTitle("Conectando");

        alertDialogBuilder
                .setMessage("Verificando conexión con servidor")
                .setCancelable(false);

        AlertDialog alertDialog = alertDialogBuilder.create();

        alertDialog.show();
    }
}
