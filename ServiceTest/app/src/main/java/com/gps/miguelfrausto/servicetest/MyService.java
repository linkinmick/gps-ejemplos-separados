package com.gps.miguelfrausto.servicetest;

import android.app.Service;
import android.content.Intent;
import android.os.Handler;
import android.os.HandlerThread;
import android.os.IBinder;
import android.util.Log;

public class MyService extends Service {

    protected static final DEFAULT_TIMEOUT = 5000;
    protected static final EXTENDED_TIMEOUT = 20000;

    private HandlerThread mBgThread;
    private Handler mBgHandler;
    private GpsApiRunnable mRunnable;

    public MyService() {
    }

    @Override
    public void onCreate() {
        super.onCreate();
        mBgThread = new HandlerThread("MyBgThread");
        mBgThread.start();
        mBgHandler = new mBgHandler(mBgThread.getLooper(), this);
        mRunnable = new GpsApiRunnable();
    }

    @Override
    public IBinder onBind(Intent intent) {
        // TODO: Return the communication channel to the service.
        throw new UnsupportedOperationException("Not yet implemented");
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        try {
            Thread.sleep(500);
            Log.e("Service", "Tread running");
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

        return START_STICKY;
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        Log.e("Service", "Servicio destruido");
    }

    private class GpsApiRunnable implements Runnable {
        @Override
        public void run() {
            //  Do whatever work you need

            //  Reschedule the Runnable
            mBgHandler.postDelayed(this, DEFAULT_TIMEOUT);
        }
    }
}
